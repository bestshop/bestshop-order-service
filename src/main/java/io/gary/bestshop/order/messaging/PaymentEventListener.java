package io.gary.bestshop.order.messaging;


import io.gary.bestshop.messaging.dto.PaymentDto;
import io.gary.bestshop.messaging.event.payment.PaymentReceivedEvent;
import io.gary.bestshop.order.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;


@Slf4j
@Component
@RequiredArgsConstructor
public class PaymentEventListener {

    private final OrderService orderService;

    @StreamListener(MessagingChannels.PAYMENT_RECEIVED_INPUT)
    public void handlePaymentReceivedEvent(PaymentReceivedEvent event) {

        log.info("Processing event: {}", event);

        PaymentDto payment = event.getPayment();

        if ("Successful".endsWith(payment.getStatus())) {
            orderService.receivePayment(payment.getOrderId(), payment.getAmount());
        }
    }
}
