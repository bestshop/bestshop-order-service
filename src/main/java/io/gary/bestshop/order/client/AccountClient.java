package io.gary.bestshop.order.client;

import io.gary.bestshop.order.domain.Account;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient("bestshop-account-service")
public interface AccountClient {

    @GetMapping("/accounts/{username}")
    Account getAccount(@PathVariable("username") String username);

}
