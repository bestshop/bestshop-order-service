package io.gary.bestshop.order.service;

import feign.FeignException;
import io.gary.bestshop.order.domain.Order;
import io.gary.bestshop.order.domain.Product;
import io.gary.bestshop.order.domain.Account;
import io.gary.bestshop.order.dto.OrderRequest;
import io.gary.bestshop.order.errors.InvalidOrderRequestException;
import io.gary.bestshop.order.errors.OrderNotCancellableException;
import io.gary.bestshop.order.errors.OrderNotCompletableException;
import io.gary.bestshop.order.errors.OrderNotFoundException;
import io.gary.bestshop.order.messaging.OrderEventPublisher;
import io.gary.bestshop.order.repository.OrderRepository;
import io.gary.bestshop.order.client.ProductClient;
import io.gary.bestshop.order.client.AccountClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static io.gary.bestshop.order.domain.OrderStatus.Cancelled;
import static io.gary.bestshop.order.domain.OrderStatus.Completed;
import static io.gary.bestshop.order.domain.OrderStatus.Created;
import static io.gary.bestshop.order.domain.OrderStatus.Delivered;
import static java.math.BigDecimal.ZERO;
import static java.util.Optional.ofNullable;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrderService {

    private final OrderRepository orderRepository;

    private final ProductClient productClient;

    private final AccountClient accountClient;

    private final OrderEventPublisher orderEventPublisher;


    public List<Order> getOrders() {

        log.info("Getting orders");

        return orderRepository.findAll();
    }

    public Order getOrder(@NotNull String id) {

        log.info("Getting order: id={}", id);

        return findOrderOrThrow(id);
    }

    public Order createOrder(@NotNull @Valid OrderRequest request) {

        log.info("Creating order: request={}", request);

        Product product = findProductOrThrow(request.getProductId());
        Account purchasedBy = findAccountOrThrow(request.getPurchasedBy());

        Order toCreate = Order.builder()
                .product(product)
                .price(product.getPrice())
                .purchasedBy(purchasedBy)
                .deliveryAddress(request.getDeliveryAddress())
                .status(Created)
                .createdAt(LocalDateTime.now())
                .build();

        Order created = orderRepository.save(toCreate);

        return orderEventPublisher.publishOrderCreatedEvent(created);
    }

    public Order cancelOrder(@NotNull String id) {

        log.info("Cancelling order: id={}", id);

        Order order = findOrderOrThrow(id);

        if (order.getStatus() != Created) {
            throw new OrderNotCancellableException(order.getStatus());
        }

        Order saved = orderRepository.save(order.withStatus(Cancelled));

        return orderEventPublisher.publishOrderCancelledEvent(saved);
    }

    public void receivePayment(@NotNull String id, @NotNull BigDecimal receivedAmount) {

        log.info("Receiving payment for order: id={}, receivedAmount={}", id, receivedAmount);

        Order order = findOrderOrThrow(id);

        BigDecimal currentReceivedAmount = ofNullable(order.getReceivedAmount()).orElse(ZERO);

        order.setReceivedAmount(currentReceivedAmount.add(receivedAmount));

        boolean shouldPublishOrderDeliveredEvent = false;
        if (order.getStatus() == Created && order.getReceivedAmount().compareTo(order.getPrice()) >= 0) {
            order.setStatus(Delivered);
            order.setDeliveredAt(LocalDateTime.now());
            shouldPublishOrderDeliveredEvent = true;
        } else {
            log.info("Cannot deliver order: {}", order);
        }

        Order updatedOrder = orderRepository.save(order);

        if (shouldPublishOrderDeliveredEvent) {
            orderEventPublisher.publishOrderDeliveredEvent(updatedOrder);
        }
    }

    public Order completeOrder(@NotNull String id) {

        log.info("Completing order: id={}", id);

        Order order = findOrderOrThrow(id);

        if (order.getStatus() != Delivered) {
            throw new OrderNotCompletableException(order.getStatus());
        }

        Order saved = orderRepository.save(order.withStatus(Completed));

        return orderEventPublisher.publishOrderCompletedEvent(saved);
    }

    private Order findOrderOrThrow(String id) {
        return orderRepository.findById(id).orElseThrow(() -> new OrderNotFoundException(id));
    }

    private Product findProductOrThrow(String productId) {
        try {
            return productClient.getProduct(productId);
        } catch (FeignException e) {
            log.warn("Error when trying to find product with id={}, reason={}", productId, e.getMessage());
            throw new InvalidOrderRequestException("Cannot find product with id: " + productId);
        }
    }

    private Account findAccountOrThrow(String username) {
        try {
            return accountClient.getAccount(username);
        } catch (FeignException e) {
            log.warn("Error when trying to find account with username={}, reason={}", username, e.getMessage());
            throw new InvalidOrderRequestException("Cannot find account with username: " + username);
        }
    }
}
