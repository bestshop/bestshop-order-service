FROM java:8-jre-alpine

EXPOSE 8080

ADD ./target/bestshop-order-service.jar /app/

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app/bestshop-order-service.jar"]